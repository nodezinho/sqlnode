import {
    Sequelize,
    Dialect
} from 'sequelize';
import dbConfig from '../config/database.json';

console.log('dbCOnfig', dbConfig);

const sequelize = new Sequelize(
    dbConfig.database, dbConfig.username, dbConfig.password, {
        dialect: dbConfig.dialect as Dialect,
        host: dbConfig.host,
        port: dbConfig.port,
        define: dbConfig.define
    }
);
sequelize.authenticate()
    .then()
    .catch(error => {
    console.log(error);
})

export default sequelize;