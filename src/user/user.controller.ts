import { Controller, Post, Body } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user ')
export class UserController {

    constructor(private userService: UserService) {}

    @Post('')
    create(@Body('name') name: string, @Body('email') email: string) {
        return this.userService.create(name, email);
    }
}


