import {
	Model,
	DataTypes
} from "sequelize";
import sequelize from "../database/connection"

export class User extends Model {
	public name: string;
	public email: string;
}


(async () => {
	
	try {
		
		User.init({
			name: DataTypes.STRING,
			email: DataTypes.STRING,
		}, {
			sequelize,
			tableName: 'users'
		});

		User.sync({
			force: true
		});
		
	} catch (err) {
		console.log('err', err);
	}

})();