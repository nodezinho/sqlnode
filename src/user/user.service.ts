import { Injectable } from '@nestjs/common';
import { User } from 'src/user/user.model';

@Injectable()
export class UserService {

    async create(name: string, email: string) {
        try {
            const user = await User.create({name, email });
            return user;
        }
        catch(err) {
            return err.mesage;
        }
    }
}
