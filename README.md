#### Nodejs Nest API + Postgres Sql On Docker Containers

- Dependencies: Docker 

* Run: 

> docker-compose up 



###### Database Commands

* Create database 
> yarn sequelize db:create

* Create migration 
> yarn sequelize migration:create --name={name}

* Create table based on migrations
> yarn sequelize db:migrate